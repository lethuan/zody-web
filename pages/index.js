import { Button } from 'antd'
import React from 'react'

// export default () => <Button className="customer-button">Primary</Button>;
class IndexPage extends React.Component {
  render() {
    return (
      <Button className="customer-button">Click me now !</Button>
    )
  }
}
export default IndexPage

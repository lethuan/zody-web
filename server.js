const express = require('express')
const next = require('next')
const MongoClient = require('mongodb').MongoClient
const bodyParser = require('body-parser')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const dbName = 'zodyapp-dev'
app.prepare().then(() => {
  let db
  const server = express()
  MongoClient.connect('mongodb://localhost:27017', (err, client) => {
    if (err) return console.log(err)
    db = client.db(dbName)
  })
  server.use(bodyParser.urlencoded({ extended: true }))
  server.use(bodyParser.json())
  server.use(express.static('public'))
  server.get('/h', (req, res) => {
    // db.collection('vouchergroups').find({
    //     $query: {
    //         city: 'da-nang',
    //         active: true,
    //     }, $orderby: { order: -1 }
    // }).toArray((err, response) => {
    //     if (err) return console.log(err)
    //     // console.log(response)
    // })
    db.collection('vouchers').find({ city: 'da-nang' }).limit(6).toArray((err, result) => {
      if (err) return console.log(err)
      return app.render(req, res, '/home', { vouchers: result })
    })
  })
  server.get('*', (req, res) => {
    return handle(req, res)
  })
  server.listen(port, () => {
    console.log('listening on 3000')
  })
})
